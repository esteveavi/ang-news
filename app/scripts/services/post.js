'use strict';
 
app.factory('Post', function ($resource) {
  return $resource('https://resplendent-fire-4740.firebaseio.com/posts/:id.json');
});
